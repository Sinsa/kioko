const rpc = require('discord-rich-presence')('1103692847573635082');
const Config = require('./config.json');
const request = require('request');
const i18nModule = require('i18n-nodejs');
const i18n = new i18nModule(Config.language, "./../../translations.json");
const createDbHafas = require('db-hafas');

const hafas = createDbHafas('kyoko');

const API_BASE_PATH = Config.trwlInstance + "/api/v1";

function updatePresence(data) {
    rpc.updatePresence({
        details: `${data.productClass} ${data.trainNumber || ''}: ${data.checkInDeparture} - ${data.checkInDestination}`,
        state: i18n.__("nextStop", {station: data.nextStation}),
        endTimestamp: data.realtimeCheckInDestinationArrival,
        largeImageKey: getImageKey(data.lineName),
        largeImageText: `${data.productClass} ${data.trainNumber || ''}: ${data.trainDeparture} - ${data.trainDestination}`,
        smallImageKey: 'traewelling',
        smallImageText: `@${data.username}`,
        instance: true
    });
}

function updatePresenceNoCheckinActive(username) {
    rpc.updatePresence({
        state: i18n.__("notTravelling"),
        largeImageKey: 'coffee',
        largeImageText: i18n.__('break'),
        smallImageKey: 'traewelling',
        smallImageText: `@${username}`,
        instance: true
    });
}

async function login() {
    console.info(`Successfully connected to Träwelling user @${Config.username}!`);
    setInterval(main, 30000);
    main();
}

async function main() {
    const activeCheckIn = await findActiveCheckin(Config.username).catch(errorCaught);
    if (activeCheckIn == null) {
        updatePresenceNoCheckinActive(Config.username);
    } else {
        const hafasTrip = await getHafasTrip(activeCheckIn.train.hafasId, activeCheckIn.train.lineName);
        preparePresence(activeCheckIn, hafasTrip, Config.username);
    }
}

async function getCheckins(username) {
    return new Promise(function (resolve, reject) {
        request({
            method: 'GET',
            uri: `${API_BASE_PATH}/user/${username}/statuses?page=1`,
        }, function (error, response, body) {
            body = JSON.parse(body);
            if (body.data.length === 0) {
                return reject("You haven't created a check-in for any journeys so far! How am I supposed to display something?");
            } else {
                return resolve(body.data);
            }
        })
    })
}

async function findActiveCheckin(username) {
    return new Promise(async function (resolve) {
        const now = new Date();
        const checkins = await getCheckins(username);
        for (const checkin of checkins) {
            // if realtime data is not available we use the plan data (useful for e.g. buses or trams)
            const departure = new Date(checkin.train.origin.departureReal || checkin.train.origin.departure);
            const arrival = new Date(checkin.train.destination.arrivalReal || checkin.train.destination.arrival);
            if (departure < now && now < arrival) {
                return resolve(checkin);
            }
        }
        return resolve(null);
    });
}

async function getHafasTrip(hafasTripId, lineNumber) {
    return new Promise(async function (resolve) {
        const trip = await hafas.trip(hafasTripId, lineNumber);
        return resolve(trip);
    });
}

function getNextStation(hafasTrip) {
    const now = new Date;
    const stopovers = hafasTrip.stopovers;
    for (let i = 1; i < stopovers.length; i++) {
        let arrivalTimePrevious = null;
        let departureTimePrevious = null;
        let arrivalTimeNext = null;
        if (stopovers[i - 1].arrival) {
            arrivalTimeNext = new Date(new Date(stopovers[i - 1].arrival).getTime() + stopovers[i - 1].arrivalDelay);
        }
        if (stopovers[i - 1].departure) {
            departureTimePrevious = new Date(new Date(stopovers[i - 1].departure).getTime() + stopovers[i - 1].departureDelay);
        }
        if (stopovers[i].arrival) {
            arrivalTimeNext = new Date(new Date(stopovers[i].arrival).getTime() + stopovers[i].arrivalDelay);
        }
        if ((arrivalTimePrevious < now && now < departureTimePrevious) || (departureTimePrevious < now && now < arrivalTimeNext)) {
            return stopovers[i].stop.name;
        }
    }
}

function preparePresence(activeCheckIn, hafasTrip, username) {
    const activeTrip = {};
    const products = activeCheckIn.train.lineName.split(' ');
    activeTrip.productClass = products[0].toUpperCase();
    activeTrip.trainNumber = products[1];
    activeTrip.checkInDeparture = activeCheckIn.train.origin.name;
    activeTrip.checkInDestination = activeCheckIn.train.destination.name;
    activeTrip.nextStation = getNextStation(hafasTrip);
    activeTrip.realtimeCheckInDestinationArrival = new Date(activeCheckIn.train.destination.arrivalReal || activeCheckIn.train.destination.arrival);
    activeTrip.trainDeparture = hafasTrip.origin.name;
    activeTrip.trainDestination = hafasTrip.destination.name;
    activeTrip.username = username;
    activeTrip.lineName = activeCheckIn.train.lineName;
    updatePresence(activeTrip)
}

function getImageKey(lineName) {
    return lineName.split(" ").join("").match(/[a-zA-z]+/g)[0].toLowerCase();
}

function errorCaught(reason) {
    console.error("ERROR:", reason);
    process.exit(1);
}

login();