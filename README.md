# Kyoko

Share your journey on https://traewelling.de with discord.

![image](https://links.sinsa.moe/resources/kyokoPreview.PNG)

Shown is the train category (e.g. ICE, IC, S), the train number (if applicable), the start and end point of your check
in aswell as of the train, the next stop and the remaining time until you need to get off.

You are able to tell the program that only check-ins with a certain visibility or higher
(e.g. only unlisted or public check-ins) should be shown on Discord.

## Installation Guide

1. Head over to https://gitlab.com/Sinsa/kyoko/-/releases/ and download the `kyoko.zip`.
2. Unpack the .zip file whereever you like
3. Change the config values (More info in the block below) so they fit for you.
4. Double-click `kyoko.exe` (or whatever OS you're using) and let it run in the background, while you are checked into a train (or not)!

## Config

| Config Key           | Description                                                                                                                        | Default / example value | Required? |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------|-------------------------|-----------|
| trwlInstance         | The Träwelling-instance you check-in to. If you don't know what this means, just leave it unchanged.                               | https://traewelling.de  | Yes       |
| username             | The username you use to log in to träwelling                                                                                       | myCoolUsername          | Yes       |
| language             | The language of the output in discord (e.g. Next stop)                                                                             | de                      | Yes       |

## Check-In Visibility

Only check-ins that are set to public in träwelling will be shown on Discord.

## Troubleshooting

### The program crashes / randomly exits!

You might have done something wrong. Try to open a cmd, go into the folder with the `kyoko.exe` file and
type `kyoko.exe`. If you have done something wrong, it will show you.

### But I really need help!

No worries. Open an issue at https://gitlab.com/sinsa/kyoko/-/issues/new or send me a direct message on
discord: https://links.sinsa.moe/discord.

## Credits

This project was heavily inspired by https://github.com/AdriDevelopsThings/iceportal_rich_presence.